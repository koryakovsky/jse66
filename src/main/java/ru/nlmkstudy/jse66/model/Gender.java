package ru.nlmkstudy.jse66.model;

public enum Gender {

    FEMALE,
    MALE
}
