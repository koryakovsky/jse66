package ru.nlmkstudy.jse66.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nlmkstudy.jse66.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
