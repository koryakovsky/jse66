package ru.nlmkstudy.jse66.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.nlmkstudy.jse66.model.Book;
import ru.nlmkstudy.jse66.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.time.LocalDateTime;

@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;

//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public CustomerDao(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }

    @Transactional
    public void saveTx(Customer customer) {

        em.merge(customer);
    }

//    @Transactional
//    public void saveTx(Customer customer) {
//
//        sessionFactory
//                .getCurrentSession().save(customer);
//    }

//    @Transactional(rollbackFor = ArithmeticException.class, noRollbackFor = NullPointerException.class)
//    public void saveCustomerAndBook() {
//        Customer customer = new Customer();
//        customer.setPhone("cusPhone");
//        customer.setEmail("email");
//        customer.setRegDate(LocalDateTime.now());
//
//        Book book = new Book("zxczxczxc", "black swan");
//
//        sessionFactory.getCurrentSession().save(customer);
//        sessionFactory.getCurrentSession().save(book);
//
//        throw new NullPointerException();
//    }

//    public <T> void save(T entity) {
//        Session session = sessionFactory.openSession();
//        Transaction tx = session.beginTransaction();
//
//        session.save(entity);
//
//        tx.commit();
//        session.close();
//    }
//
//    public <T> T getById(Class<T> entityClass, Serializable id) {
//        Session session = sessionFactory.openSession();
//        Transaction tx = session.beginTransaction();
//
//        T entity = session.find(entityClass, id, LockModeType.PESSIMISTIC_WRITE);
//
//        tx.commit();
//        session.close();
//
//        return entity;
//    }


}
