package ru.nlmkstudy.jse66;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.nlmkstudy.jse66.config.PersistenceConfig;
import ru.nlmkstudy.jse66.dao.CustomerDao;
import ru.nlmkstudy.jse66.model.Customer;
import ru.nlmkstudy.jse66.repository.CustomerRepository;

import java.time.LocalDateTime;
import java.util.List;

public class App {

    public static void main(String[] args) {

        ApplicationContext context =
                new AnnotationConfigApplicationContext(PersistenceConfig.class);

        CustomerRepository repository = context.getBean(CustomerRepository.class);

        for (Customer customer : repository.findAll()) {
            System.out.println(customer);
        }


    }
}
